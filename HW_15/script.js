// 1. Найти площадь прямоугольника

function square(a, b) {
    return a * b;
}
console.log(square(3,7));
square(3,7);

let square = function(a, b) {
    return a * b;
}
console.log(square(2,4));
square(2,4);

let square = (a, b) => (a * b)
console.log(square(2,2));
square(2,2);

// 2. Теорема Пифагора

function pif_paf(a, b) {
    return Math.sqrt(a*a + b*b);
}
console.log(pif_paf(3,4));
pif_paf(3,4);

let pif_paf = function(a, b) {
    return Math.sqrt(a*a + b*b);
}
console.log(pif_paf(3,4));
pif_paf(3,4);

let pif_paf = (a, b) => Math.sqrt(a*a + b*b)
console.log(pif_paf(3,4));
pif_paf(3,4);

// 3. Найти дискриминант

function dsct(a, b, c) {
    return (b*b)-4*a*c;
}
console.log(dsct(4,2,2));

let dsct = function(a, b, c) {
    return (b*b)-4*a*c;
}
console.log(dsct(4,2,2));

let dsct  = (a, b, c) => (b*b)-(4*a*c)
console.log(dsct(4,2,2));

// 4.Создать только четные числа до 100

function even(count) {
    let numbers = []
    for (let i = 0; i <= count; i++) {
        if (i % 2 === 0) {
            numbers.push(i) 
    }   
}
    return numbers;  
}
console.log(even(200));

let even = function(count) {
    let numbers = []
    for (let i = 0; i <= count; i++) {
        if (i % 2 === 0) {
            numbers.push(i);
        }  
    }
    return numbers; 
}
console.log(even(200));

let even = (count) => {
    let i;
    let numbers = []
    for (let i = 0; i <= count; i++) {
        if (i % 2 === 0) {
            numbers.push(i);
    }  
}
    return numbers;  
}
console.log(even(200));

function chet(arr){
    let result = []
    let i = 0
    for (let val of arr) {
        if (val % 2 ===0) {
        result[i] = val
        i++
    }
}
    return result 
}
let aaa = []
for (let i = 0; i <= 100; i++) {
    aaa.push(i)
}
console.log(chet(aaa));

function chet(arr){
        let result = []
        let i = 0
    for (let index in arr) {
        if (arr[index] % 2 ===0) {
        result[i] = arr[index]
        i++
        }
    }
    return result 
}
    let aaa = []
        for (let i = 0; i <= 100; i++) {
        aaa.push(i)
}
console.log(chet(aaa));

// 5. Создать нечетные числа до 100

function even(count) {
    let i;
    let numbers = []
    for (let i = 0; i <= count; i++) {
        if (i % 2 !== 0) {
            numbers.push(i); 
    }   
}
    return numbers;  
}
console.log(even(200));

let even = function(count) {
    let i;
    let numbers = []
    for (let i = 0; i <= count; i++) {
        if (i % 2 !== 0) {
            numbers.push(i);
    }   
}
    return numbers;    
}
console.log(even(200));

let even = (count) => {
    let i;
    let numbers = []
    for (let i = 0; i <= count; i++) {
        if (i % 2 !== 0) {
            numbers.push(i);
    }   
}
    return numbers;        
}
console.log(even(200));

// 6.Создать функцию по нахождению числа в степени

function power(a, b) {
    return a**b;
}
console.log(power(3,3));

let power = function(a, b) {
    return a**b;
}
console.log(power(4,2));  

let power  = (a, b) => a**b
console.log(power(4,2));

// 7 Написать функцию сортировки.
// Функция принимает массив случайных чисел и сортирует их по порядку.
// По дефолту функция сортирует в порядке возростания. 
// Но если передать второй параметр то функция будет сортировать
// по убыванию.
// sort(arr)
// sotrt(arr, 'asc')
// sotrt(arr, 'desc')

let mass = []
for (let i = 0; i < 10; i++) {
  mass.push(Math.floor(Math.random() * 20))
}
let sortMain = function (arr, val='asc') {
  let leng = arr.length
  for (let j = 0; j < leng; j++) {
    let b
    for (let z = 0; z < leng; z++) {
      if (val === 'asc') {
        if(arr[j] < arr[z]){
            b = arr[z]
            arr[z] = arr[j]
            arr[j] = b
        }
      } else if (val == 'desc') {
        if(arr[j] > arr[z]){
            b = arr[z]
            arr[z] = arr[j]
            arr[j] = b
        }
      }
    }
  }
  return arr
}
console.log(sortMain(mass))
console.log(sortMain(mass,'asc'))
console.log(sortMain(mass,'desc'))

// 8 Написать функцию поиска в массиве.
// функция будет принимать два параметра. 
// Первый массив, второй посковое число. search(arr, find)

function search (arr, find) {
    for (let value of arr) {
     return true; }
     return false;}
    let mas = [12, 33, 44, 55, 66, 77]
        console.log(search(mas, 12))
            function search (arr, find) {
                for(let value of arr) {
                    if (value === find) {
        return true;}
    }
   return false;
}
console.log(search(mas, 71))