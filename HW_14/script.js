// Написати функцію, що буде сортувати числа, наприклад від 1 до 20 
// та вивести сортовані числа на екран

function makeArr(arrLength) {
    let arr = [];
    for(let i = 0; i < arrLength; i++) {
        arr.push(Math.floor(Math.random() * 20));
    }
    arr.sort();
    return arr   
}
console.log(makeArr(20));
makeArr();

// Є масив об'єктів
// salaries [{name:'ivan', salary:300}, {name:'vova', salary:500},
// {name:'zigmund', salary:1300}], що містять заробітні плати.
// Напишіть функцію sumSalaries(salaries), яка повертає суму всіх зарплат
// - використовуючи вбудовані функції масивів .
// Якщо об'єкт salaries порожній, то результат повинен бути 0.
// Виведіть на екран результат.

let salaries = [{name:'ivan', salary:300}, {name:'vova', salary:500}, {name:'zigmund', salary:1300}];
function sort(salaries) {
    let sorted = {
        high: [],
        middle: []
    }
    for (let key in salaries) {
        if (salaries[key].salary >= 300 && salaries[key].salary <= 600) {
            sorted.middle.push(salaries[key]);
        } else if (salaries[key].salary >= 1000 && salaries[key].salary <= 2000) {
            sorted.high.push(salaries[key]);
        }
    }
    return sorted;
}
let sortedArray = sort(salaries);
console.log(sortedArray);
sort();

// Створити функцію, що в залежності від зарплати групує співробітників 
// 300-600 середній оклад, 
// 1000-2000 - високий:отримати об'єкт {high:[people with salary], 
// middle:[people with salary], 
// low:[ppl with salary].
// Якщо категорія пуста, має бути пустий масив.

let salaries = [{name:'ivan', salary:300}, {name:'vova', salary:500}, {name:'zigmund', salary:1300}];
function sort(salaries) {
    let sorted = {
        high: [],
        middle: []
    }
    let sum = 0;
    for (let key in salaries) {
        if (salaries[key].salary >= 300 && salaries[key].salary <= 600) {
            sorted.middle.push(salaries[key]);
        } else if (salaries[key].salary >= 1000 && salaries[key].salary <= 2000) {
            sorted.high.push(salaries[key]);
        }
        sum += salaries[key].salary;
    }
    return {sorted, sum};
}
let sortedArray = sort(salaries);
console.log(sortedArray);