// 1.Дан массив
// ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
// Развернуть этот массив в обратном направлении

let arr = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
let ress = [];
let len11 = arr.length-1;
for (let a = len11; a>=0; a--) {
    ress.push(arr[a]);
}
console.log(ress);

let arrNames = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
let resNames = [];
let d = arrNames.length-1;
while (d >= 0) {
    resNames.push(arrNames[d]);
    d--;  
} 
console.log(resNames);

let arrN = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
let resN = [];
let g = arrNames.length-1;
do {
    resN.push(arrN[g]);
    g--;  
}
while (g >= 0) {
} 
console.log(resN);

let arrM = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
let ressM = [];
for (let value of arrM) {
   ressM.unshift(value);
}
console.log(ressM);

let arrM1 = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
let ressM1 = [];
for (let key in arrM1) {
    ressM1.unshift(arrM1[key]);
}
console.log(ressM1);

//  2. Дан массив
// [44, 12, 11, 7, 1, 99, 43, 5, 69]
// Развернуть этот массив в обратном направлении

let arrNumbers = [44, 12, 11, 7, 1, 99, 43, 5, 69];
let result = [];
let len10 = arrNumbers.length-1;
for (let i = len10; i>=0; i--) {
    result.push(arrNumbers[i]);
} 
console.log(result);

let arrNumb = [44, 12, 11, 7, 1, 99, 43, 5, 69];
let res = [];
let r = arrNumb.length-1;
while (r >= 0) {
    res.push(arrNumb[r]);
    r--;  
} 
console.log(res);

let arrNum = [44, 12, 11, 7, 1, 99, 43, 5, 69];
let resl = [];
let f = arrNum.length-1;
do {
    resl.push(arrNum[f]);  
    f--;
}
while (f >= 0);
console.log(resl);  

let arrNum1 = [44, 12, 11, 7, 1, 99, 43, 5, 69];
let resNum1 = [];
for (let value of arrNum1) {
    resNum1.unshift(value);
}
console.log(resNum1);

let arrNum2 = [44, 12, 11, 7, 1, 99, 43, 5, 69];
let resNum2 = [];
for (key in arrNum2) {
    resNum2.unshift(arrNum2[key]);
}
console.log(resNum2);

// 3. Дана строка
// let str = 'Hi I am ALex'
// развенуть строку в обратном направлении.

let arrStr = 'Hi I am ALex';
let res1 = '';
let len9 = arrStr.length-1
for (let h = len9; h>=0; h--) {
    res1 += arrStr.charAt(h);
}
console.log(res1);

let arrSt = 'Hi I am ALex';
let res2 = '';
let k = arrSt.length-1;
while (k >=0) {
    res2 += arrSt.charAt(k);
    k--;
}
console.log(res2);

let arrSt1 = 'Hi I am ALex';
let res21 = '';
let k1 = arrSt1.length-1;
do {
    res21 += arrSt1.charAt(k1);
    k1--;
}
while (k1 >= 0);
console.log(res21);

let arrSt2 = 'Hi I am ALex';
let res22 = '';
let k2 = arrSt2.length-1;
for(let value of arrSt2) {
    res22 += arrSt2.charAt(k2);
    k2--;
}
console.log(res22);

let arrSt3 = 'Hi I am ALex';
let res23 = '';
let k3 = arrSt1.length-1;
for(let key in arrSt3) {
    res23 += arrSt3.charAt(k3);
    k3--;
}
console.log(res23);
     
// 4. Дана строка 
// let str = 'Hi I am ALex'
// сделать ее с с маленьких букв

let str1 = 'Hi I am ALex';
let res4 = '';
for (let z = 0; z>=0; z--) {
    res4 += str1.toLowerCase(z);
}
console.log(res4);

let st = 'Hi I am ALex';
let res5 = '';
let x = 0;
while (x >=0) {
    res5 += st.toLowerCase(x);
    x--;
}
console.log(res5);

let strr = 'Hi I am ALex';
let res6 = '';
let c = 0;
do {
    res6 += strr.toLowerCase(c);
    c--;
}
while (c >= 0);
console.log(res6);

let strr1 = 'Hi I am ALex';
let strr1Elem;
let newstrr1 = [];
for(let value of strr1) {
    strr1Elem = value.toLowerCase();
    newstrr1 += strr1Elem;
}
console.log(newstrr1);

let strr2 = 'Hi I am ALex';
let strr2Elem;
let newstrr2 = '';
for (let key in strr2) {
    strr2Elem = strr2[key].toLowerCase();
    newstrr2 += strr2Elem;
}
console.log(newstrr2);

//  5.Дана строка 
// let str = 'Hi I am ALex'
// сделать все буквы большие

let str2 = 'Hi I am ALex';
let res7 = '';
for (let v = 0; v>=0; v--) {
    res7 += str2.toUpperCase(v);
}
console.log(res7);

let str3 = 'Hi I am ALex';
let res8 = '';
let b = 0;
while (b >=0 ) {
    res8+= str3.toUpperCase(b);
    b--;
}
console.log(res8);

let str4 = 'Hi I am ALex';
let res9 = '';
let m = 0;
do {
    res9+= str4.toUpperCase(m);
    m--;
}
while (m >=0);
console.log(res9);

let strr5 = 'Hi I am ALex';
let strr5Elem;
let newstrr5 = [];
for(let value of strr5) {
    strr5Elem = value.toUpperCase();
    newstrr5 += strr5Elem;
}
console.log(newstrr5);

let strr6 = 'Hi I am ALex';
let strr6Elem;
let newstrr6 = '';
for (let key in strr6) {
    strr6Elem = strr6[key].toUpperCase();
    newstrr6 += strr6Elem;
}
    console.log(newstrr6);

// 6.Дан массив
// ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
// сделать все буквы с маленькой

let arr1 = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
let res10 = [];
let len8 = arr1.length;
for (let qq = 0; qq<= len8 - 1; qq++) {
    res10.push(arr1[qq].toLowerCase());
}
console.log(res10);

let arr2 = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
let res11 = [];
let as = 0;
let len7 = arr2.length;
while (as <= len7 - 1) {
    res11.push(arr2[as].toLowerCase());
    as++;
}
console.log(res11);

let arr3 = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
let res12 = [];
let as1 = 0;
let len6 = arr3.length;
do {
    res12.push(arr3[as1].toLowerCase());
    as1++;
}
while (as1 <= len6 - 1);
console.log(res12);

let arr31 = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
let res121 = [];
let as11 = 0;
for (let value of arr31) {
    res121.push(arr31[as11].toLowerCase());
    as11++;
}
console.log(res121);

let arr32 = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
let res122 = [];
let as12 = 0;
for (let key in arr32) {
    res122.push(arr32[as12].toLowerCase());
    as12++;
}
console.log(res122);

// 7.Дан массив
// ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
// сделать все буквы с большой

let arr4 = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
let res13 = [];
let len5 = arr4.length;
for (let jj = 0; jj <= len5 -1; jj++) {
    res13.push(arr4[jj].toUpperCase());
}
console.log(res13);

let arr5 = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
let res14 = [];
let asd = 0;
let len4 = arr5.length;
while (asd <= len4 - 1) {
    res14.push(arr5[asd].toUpperCase());
    asd++;
}
console.log(res14);

let arr6 = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
let res15 = [];
let as2 = 0;
let len3 = arr6.length;
do {
    res15.push(arr6[as2].toUpperCase());
    as2++;
}
while (as2 <= len3 - 1);
console.log(res15);

let arr61 = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
let res151 = [];
let as21 = 0;
for (let value of arr61) {
    res151.push(arr61[as21].toUpperCase());
    as21++;
}
console.log(res151);

let arr62 = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya'];
let res152 = [];
let as22 = 0;
for (let key in arr62) {
    res152.push(arr62[as22].toUpperCase());
    as22++;
}
console.log(res152);

// 8. Дано число 
// let num = 1234678 
// развернуть его в обратном направлении

let n = 1234678;
let result_n = 0;
while (n) {
    result_n *= 10;
    result_n += n % 10;
    n = Math.floor(n / 10);
}
console.log(result_n);

let n1 = 1234678;
let result_n1 = 0;
for (; n1>0; n1 = Math.floor(n1 / 10)){
    result_n1 *= 10;
    result_n1 += n1 % 10;
}
console.log(result_n1);

let n2 = 1234678;
let result_n2 = 0;
do {
    result_n2 *= 10;
    result_n2 += n2 % 10;
    n2 = Math.floor(n2 / 10);
}
while ( n2 > 0);  
console.log(result_n2);

let n3 = 1234678;
let result_n3 = '';
for (let key of n3.toString()) {
    result_n3 = key + result_n3;
}
console.log(+result_n3);

let n4 = 1234678;
let result_n4 = '';
for (let key in n4.toString()) {
    result_n4 = n4.toString()[key]  + result_n4;
}
console.log(+result_n4);

// 10. Дан массив 
// [44, 12, 11, 7, 1, 99, 43, 5, 69]
// отсортируй его в порядке убывания
// не используй готовые функции

let arrr = [44, 12, 11, 7, 1, 99, 43, 5, 69];
let len2 = arrr.length;
for (let i = 0; i < len2; i++) {
    for (let j = 0; j < len2; j++) {
        if (arrr[i] > arrr[j]) {
            let t = arrr[i];
            arrr[i] = arrr[j];
            arrr[j] = t;
        }
    }
}
console.log(arrr);

let arr7 = [44, 12, 11, 7, 1, 99, 43, 5, 69];
let ia = 0;
let len1 = arr7.length;
while (ia < len1) {
    ja = ia;
    while (ja < len1) {
        if (arr7[ia] < arr7[ja]) {
            let ta = arr7[ia];
            arr7[ia] = arr7[ja];
            arr7[ja] = ta; 
        }
        ja++;
    }
    ia++;
}
console.log(arr7);

let arr8 = [44, 12, 11, 7, 1, 99, 43, 5, 69];
let iaa = 0;
let len = arr8.length;
do {
    jaa = iaa;
    do {
        if (arr8[iaa] < arr8[jaa]) {
            let taa = arr8[iaa];
            arr8[iaa] = arr8[jaa];
            arr8[jaa] = taa; 
        }
        jaa++;
    }
    while (jaa < len)
    iaa++;
}
while (iaa < len)
console.log(arr8);

let arr9 = [44, 12, 11, 7, 1, 99, 43, 5, 69];
for (let iaaa in arr9) {
    for (jaaa in arr9) {
        if (arr9[jaaa] < arr9[iaaa]) {
            mas = arr9[iaaa];
            arr9[iaaa] = arr9[jaaa];
            arr9[jaaa] = mas;
        }
    }
}
console.log(arr9);

// 2 часть 

// 1. Данн массив чисел [12, 22, -1245, -1, 0, 4994, 555]
// -Определить минимальное число
// -Определить максимальное число
// -Определить среднее число между этих чисел

let numbers = [12, 22, -1245, -1, 0, 4994, 555];
let min, max;
let sum=0;
let i = 0;
while (i < numbers.length) {
    if (!min || !max) {
        min = numbers[i]; max = numbers[i];
     }
    if (numbers[i] <= min) {
            min = numbers[i];
    } else if (numbers[i] >= max) 
    {
    max = numbers[i];
    }
    sum += numbers[i];
    result = Math.floor(sum / numbers.length);
    i++;
    }
console.log(min, max, result);

let numbers = [12, 22, -1245, -1, 0, 4994, 555];
let min, max;
let sum=0;
for (let i = 0; i < numbers.length; i++) {
     if (!min || !max) {
     min = numbers[i]; max = numbers[i];
    }
     if (numbers[i] <= min) {
     min = numbers[i];
    } else if (numbers[i] >= max) 
    {
     max = numbers[i];
    }
     sum += numbers[i];
    }
result = Math.floor(sum / numbers.length);  
console.log(min, max, result);

let numbers = [12, 22, -1245, -1, 0, 4994, 555];
let min, max;
let sum=0;
let i = 0;
do {
   if (!min || !max) {
   min = numbers[i]; max = numbers[i];
   }
   if (numbers[i] <= min) {
   min = numbers[i];
   } else if (numbers[i] >= max )
   {
   max = numbers[i];
   }
   sum += numbers[i];
   result = Math.floor(sum / numbers.length);
   i++;     
   }
while (i < numbers.length);
console.log(min, max, result);

let numbers = [12, 22, -1245, -1, 0, 4994, 555];
let min, max;
let sum=0;
for (let key in numbers) {
    if (!min || !max) {
    min = numbers[key]; max = numbers[key];
  }
    if (numbers[key] <= min) {
    min = numbers[key];
  } else if (numbers[key] >= max)
    {
    max = numbers[key];
  }
    sum +=numbers[key];
}
    result = Math.floor(sum / numbers.length);      
console.log(min, max, result);

let numbers = [12, 22, -1245, -1, 0, 4994, 555];
let min, max;
let sum=0;
for (let key of numbers) {
    if (!min || !max) {
    min =key; max = key;
  }
    if (key <= min) {
    min = key;
  } else if (key >= max)
    {
    max = key;
  }
    sum +=key;
}
    result = Math.floor(sum / numbers.length);      
console.log(min, max, result);

// 2. Дан массив чисел [12, 22, -1245, -1, 0, 4994, 555]
// -Посчитать длину массива, нельзя использовать .length

let arr = [12, 22, -1245, -1, 0, 4994, 555];
let length = 0;
let i = 0;
while (arr[i]!==undefined) {
    i++;
    length++;
}
console.log(length);

let arr = [12, 22, -1245, -1, 0, 4994, 555];
let length = 0;
for (i = 0; arr[i]!==undefined; i++) {
    length++;
}
console.log(length);

let arr = [12, 22, -1245, -1, 0, 4994, 555];
let length = 0;
let i = 0;
do {
    i++;
    length++;
}
while (arr[i]!==undefined);
console.log(length);

let arr = [12, 22, -1245, -1, 0, 4994, 555];
let length = 0;
for(let value of arr) {
    length++;
}
console.log(length); 

let arr = [12, 22, -1245, -1, 0, 4994, 555];
let length = 0;
for (let key in arr) {
    length++;
}
console.log(length);

// 3. Данн массив чисел [12, 22, -1245, -1, 0, 4994, 555]
// -Сделать положительные числа отрицательными
// -Сделать отрицательные числа положительными

let arr = [12, 22, -1245, -1, 0, 4994, 555];
let i = 0;
let len = arr.length;
while (i < len) {
    arr[i] *= -1;
    i++;
}
console.log(arr);

let arr = [12, 22, -1245, -1, 0, 4994, 555];
let len = arr.length;
for ( let i = 0; i < len; i++) {
    arr[i] *= -1;
}
console.log(arr);

let arr = [12, 22, -1245, -1, 0, 4994, 555];
let i = 0;
let len = arr.length;
do {
    arr[i] *= -1;
    i++;
} while (i < len);
console.log(arr);

let arr = [12, 22, -1245, -1, 0, 4994, 555];
let i = 0;
for (let value of arr) {
    arr[i] *= -1;
    i++
}
console.log(arr);

let arr = [12, 22, -1245, -1, 0, 4994, 555];
let i = 0;
for (let key in arr) {
    arr[i] *= -1;
    i++
}
console.log(arr);

// 4. Дан массив чисел [12, 2, 7, 21, 22, -1245, -1, 0, 4994, 555]
// -удалить только отрицательные числа
// -удалить все числа которые больше 12

let arr = [12, 2, 7, 21, 22, -1245, -1, 0, 4994, 555];
for ( let i = arr.length-1; i >= 0; i--) {
    if (arr[i] < 0) {
        arr.splice(i, 1);
    }
    else if (arr[i] > 12) {
        arr.splice(i, 1);
  }
}
console.log(arr);

let arr = [12, 2, 7, 21, 22, -1245, -1, 0, 4994, 555];
let i = arr.length-1;
while (i >= 0) {
    if (arr[i] < 0) {
        arr.splice(i, 1);
    }
    else if (arr[i] > 12) {
    arr.splice(i, 1);

  }
    i--;
}
console.log(arr);

let arr = [12, 2, 7, 21, 22, -1245, -1, 0, 4994, 555];
let i = arr.length-1;
do {
    if (arr[i] < 0) {
        arr.splice(i, 1);
    }
    else if (arr[i] > 12) {
    arr.splice(i, 1);
  }
    i--;
}
while (i >= 0);
console.log(arr);

let arr = [12, 2, 7, 21, 22, -1245, -1, 0, 4994, 555];
arr1 = [];
arr2 = [];
for (key in arr) {
    if ((arr[key] > 0) || (arr[key] == 0)) {
        arr1.push(arr[key]);
    }
     if (arr[key] > 12) {
     arr2.push(arr[key]);
  }  
}
console.log(arr1);
console.log(arr2);

let arr = [12, 2, 7, 21, 22, -1245, -1, 0, 4994, 555];
arr1 = [];
arr2 = [];
for (value of arr) {
    if (value >= 0) {
    arr1.push(value);
    }
    if (value > 12) {
    arr2.push(value);
  }
}
  console.log(arr1);
  console.log(arr2);


// 5. Дан массив массивов 

let arr = [
    [
    car = 'Nissan',
    model = 'Leaf',
    color = 'green',
    year = 2015,
    engine = 1.6
    ],
    [
    car = 'bmw',
    model = 'i3',
    color = 'black',
    year = 2018,
    engine = 2.0
    ],
    [
    car = 'bmw',
    model = 'x5',
    color = 'orange',
    year = 2016,
    engine = 3.0
    ],
    [
    car = 'mersedes',
    model = 'e220',
    color = 'blue',
    year = 2014,
    engine = 2.0
    ],
    [
    car = 'волга',
    model = '2410',
    color = 'black',
    year = 1989,
    engine = 2.4
    ],
    ]

// Отсортировать массивы по годам, 
// Нельзя использовать готовые функции

let i = 0;
let a;
let leng = arr.length;
while (i < leng) {
  let y = 0;
  while (y < leng) {
    if (arr[i][3] < arr[y][3]) {
      a = arr[i];
      arr[i] = arr[y];
      arr[y] = a;
    }
    y++;
  }
  i++;
}
console.log(arr);

let i = 0;
let a;
let leng = arr.length;
for (let i = 0; i < leng; i++) {
    let y = 0;
for (let y = 0; y < leng; y++)
if (arr[i][3] < arr[y][3]) {
          a = arr[i];
          arr[i] = arr[y];
          arr[y] = a;
  }
}
console.log(arr);

let i = 0;
let a;
let leng = arr.length;
do {
   y = 0;
do {
    if (arr[i][3] < arr[y][3]) {
        a = arr[i];
        arr[i] = arr[y];
        arr[y] = a;       
    }
        y++;
  }
while (y < leng)
        i++;
}
while (i < leng ) 
console.log(arr);

let len = 2020;
sortArr = [];
for (let i = 0; i < len; i++) {
  for (let key in arr){
    if (arr[key][3] == i) {
      sortArr.push(arr[key]);
    }
  }
}
console.log(sortArr);

let len = 2020;
sortArr = [];
for (let i = 0; i < len; i++) {
  for (let value of arr){
    if (value[3] == i) {
      sortArr.push(value);
    }
  }
}
console.log(sortArr);