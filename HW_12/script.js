// 1.Переменные

let a = 3;
alert(a);

let f = 10;
let b = 2;
alert(f + b);
alert(f - b);
alert(f * b);
alert(f / b);

let c = 15;
let d = 2;
let result = (c + d);
alert(result);

let s = 10;
let u = 2;
let t = 5;
alert(s + u + t);

let a1 = 17;
let b1= 10;
let c1 = a1 - b1;
let d1 = 7;
let result2 = c1 + d1;
alert(result2);

// 2.Строки

let text = 'Привет,Мир!';
alert(text);

let text1 = 'Привет,';
let text2 = 'Мир!';
alert(text1 + text2);

let sec = 60;
let m = 60;
let hour = sec * m;
alert(hour);
let day = 24;
let sec_Day = hour * day;
alert(sec_Day);
let week = 7;
let sec_Week = week * sec_Day;
alert(sec_Week);
let month = 30;
let sec_Month = month * sec_Day;
alert(sec_Month);

let num = 1;
num += 12;
num -= 14;
num *= 5;
num /= 7;
num++;
num--;
console.log(num);

let now = new Date();
let hour1 = now.getHours();
let min1 = now.getMinutes();
let sec1 = now.getSeconds();
console.log(`${hour1} : ${min1} : ${sec1}`);

let Text = 'Я';
Text +=' хочу' ;
Text +=' знать' ;
Text +=' JS!' ;
console.log(Text);

let foo = 'bar';
let bar = 10;
alert (foo = bar);

// Еще немного арифметики посложнее

let n1 = 2;
let n2 = 3;
let nsum = n1 + n2;
let nmulti = n1 * n2;
console.log(nsum, nmulti);

let n3 = 9;
let n4 = 16;
let nsquare = n3*n3 + n4*n4;
console.log(nsquare);

let n5 = 2;
let n6 = 3;
let n7 = 4;
let average = (n5 + n6 + n7)/3;
console.log(average);

let x = 1;
let y = 2;
let z = 3;
let xyz = (x+1)-2*(z-2*x+y);
console.log(xyz);

let nn = 8;
let nn1 = nn % 3;
let nn2 = nn % 5;
console.log(nn1, nn2);

let n8 = 7;
let percent30 = 0.3;
let percent120 = 1.2;
let n9 = n8 * percent30;
let n10 = n9 + n8;
let n11 = n8 * percent120;
let n12 = n8 + n11;
console.log(n10, n12);

let n13 = 10;
let n14 = 20;
let percent40 = 0.4;
let percent84 = 0.84;
let n13n = n13 * percent40;
let n14n = n14 * percent84;
let n15n = n13n + n14n;
console.log(n15n);

let a3 = 313
let b3 = String(a3)
let sum = ((+b3[0]) +(+b3[1]) + (+b3[2]))
console.log(sum)

let aa = 214
let res_aa = +(aa.toString()[0] + 0 + aa.toString()[2])
console.log(res_aa)

let c_1 = 963, resc1 = 0;
while (c_1) {
    resc1 *= 10;
    resc1 += c_1 % 10;
    c_1 = Math.floor(c_1 / 10);
}
console.log(resc1);

let av = 1;
let bv = 3;
console.log(av++ + bv); //будет 4 (потому что av++ = еще 1 добавляем 3)
console.log(av + ++bv); //будет 6 (потому что av уже равно 2 и + 4( 1+bv)
console.log(++av + bv++); //будет 7 (потому что 3 (av было 2 и ++ - это плюс1) + 4(b тут уже 4)

let age = 18;
console.log(age >= 18 ? 'Тебе есть 18 лет' : 'Тебе нет 18 лет');

let p = true;
console.log(p ? 'Есть загран паспорт' : 'Нет загран паспорта')

let yourAge = 16;
console.log(yourAge == 16 ? 'Тебе уже есть 16 лет' : 'Тебе не 16 лет')

let mod = prompt("Введите число" , 0);
if (mod % 2 ==0) {
    alert("Число"+ "-" + mod + "-" + "Четное");
}else{
    alert("Число" + "-" + mod + "-" + "Нечетное");
}

// 1.	Дано два числа 42 и 55 определите при помощи тернарной операции какое число больше.
// -	Используй в место статических чисел функцию rand(); Пример я ограничил набор случайных чисел от 5 до 15
// rand(5, 15);
// -	Определи минимальное и максимальное число

let fn = 42, sn = 55;
let tn = (fn > sn) ? fn : sn;
console.log(tn);

let rand = Math.floor(Math.random()*(15 - 5) + 1);
console.log(rand);

let randm = [5,6,7,8,9,10,11,12,13,14,15];
let max = Math.max.apply(null, randm);
console.log(max);
let min = Math.min.apply(null, randm);
console.log(min);

// 2.	Сокращение Имени и Отчества. Возьмите за основу свою ФИО. У вас будет 3 переменные. (Ф.И.О.)
// Ваша программа должна сократить Имя и Отчество. вот пример:
// (Иванов Иван Иванович) => (Иванов И. И.)

let name = 'Евгения';
let sname = 'Алексеенко';
let fname = 'Станиславовна';
console.log(sname + ' ' + '' + name[0]+ '.' + fname[0] + '.')

// 3.	Даны 4-ри разных числа (a=12 b=14 c=10 d=-12) Программа должна быть универсальной, и должна подходить для
// масштабирования. Задачей алгоритма определить какой число минимальное какое максимальное.
// НЕЛЬЗЯ ИСПОЛЬЗОВАТЬ ГОТОВЫЕ ФУНКЦИИ!!!
// - Дано 6-разных чисел (придумай сам)

let numbers = [12, 47, -12, 36];
let minN, maxN;
let i = 0;
let len = numbers.length;
while(i < len) {
    if (!minN || !maxN) {
        minN = numbers[i]; maxN = numbers[i];
    }
    if (numbers[i] <= minN) {
        minN = numbers[i];
    } else if (numbers[i] >= maxN
    ) {
        maxN = numbers[i];
    }
    i++;
}
console.log(minN, maxN);

let numbersN = [1, 8, 3, 50, 25, 3];
let minNN, maxNN;
let i_n = 0;
let lenn = numbersN.length;
while(i_n < lenn) {
    if (!minNN || !maxNN) {
        minNN = numbersN[i_n]; maxNN = numbersN[i_n];
    }
    if (numbersN[i_n] <= minNN) {
        minNN = numbersN[i_n];
    } else if (numbersN[i_n] >= maxNN
    ) {
        maxNN = numbersN[i_n];
    }
    i_n++;
};
console.log(minNN, maxNN);

// 4.	Даны три стороны треугольника. Алгоритм должен определить и вывести сообщение какой это треугольник.

let stor1 = 8;
let stor2 = 9;
let stor3 = 10;

let res;
if (stor1 == stor2 || stor3 == stor2) {
    res = "Равнобедреный треугольник";
    if (stor1 == stor3) {
        res = "Равносторонний треугольник";
    }
} else if (stor1 == stor3) {
    res = "Равнобедреный треугольник";
} else {
    res = "Разносторонний треугольник";
}
console.log(res);

//5.	Известны 4 стороны. Программа должна выводить прямоугольник или квадрат.
// Также продумать исключения является ли это вообще фигурой.

let storr1 = 8;
let storr2 = 8;
let storr3 = 8;
let storr4 = 8;

let ress;
if (storr1 == storr2 && storr3 == storr4) {
    ress = "Это прямоугольник";
    if (stor1 == storr4) {
        ress = "Это квадрат";
    }
} else if (storr1 == storr2 || storr3 == storr4) {
    ress = "Это равнобедренная трапеция";
} else if (storr1 <= 0 || storr2 <= 0 || storr3 <=0 || storr4 <=0) {
    ress = "Это не фигура";
} else {
    ress = "Это не квадрат, и не прямоугольник;)";
}
console.log(ress);

// 6.	Вам нужно разработать программу, которая считала бы количество вхождений какой-нибудь выбранной вами цифры в
// выбранном вами числе. Например: цифра 7 в числе 123456789 встречается 1 раза
// (ограничите себя функцией rand(1, 99999) – это ваше случайное число) используй функцию substr_count()

let str_l = '1234556789';
let len = str_l.length;
let search = 5;
let i_l = 0;
let result_len = 0;
while (i_l < len) {
    if (search == str_l[i_l]) {
result_len++
    }

    i_l++
}
console.log(result_len);

// 7.	В переменной month лежит какое-то число из интервала от 1 до 12. Определите в какую пору года попадает этот
// месяц (зима, лето, весна, осень).

let month2 = 12;
if (month2 >=3 && month2 < 6) {
    console.log('Весна');
}
else if (month2 >= 6 && month2 < 9) {
    console.log('Лето');
}
else if  (month2 >= 9 && month2 < 12) {
    console.log('Осень');
}
else if (month2 >= 1 && month2 < 3 || month2 == 12) {
    console.log('Зима');
}

let month1 = prompt('Введите число', '');
console.log(`Число попадает в ${Math.floor(month1/3) + 1} четверть`)

// 8.	Дана строка, состоящая из символов, например, 'abcde'. Проверьте, что первым символом этой строки является буква
// 'a'. Если это так - выведите 'да', в противном случае выведите 'нет'.

let str = 'abcd';
if (str[0] === 'a') {
    console.log('Yes')
} else {
    console.log('No')
}

let srt0 = 'abcd';
(srt0[0] === 'a') ? console.log('Верно') : console.log('Неверно');

// 9.	Дана строка с цифрами, например, '12345'. Проверьте, что первым символом этой строки является цифра 1, 2 или 3.
// Если это так - выведите 'да', в противном случае выведите 'нет'.

let str2 = '12345';
if (str2[0] == '1' || str2[0] == '2' || str2[0] == '3') {
    console.log('Yes')
} else {
    console.log('No')
}
let str3 = '12345';
(str3[0] == '1' || str3[0] == '2' || str3[0] == '3') ? console.log('Верно') : console.log('Неверно');

// 10.	Если переменная test равна true, то выведите 'Верно', иначе выведите 'Неверно'. Проверьте работу скрипта при
// test, равном true, false. Напишите два варианта скрипта - тернарка и if else.

let test = true;
test ? console.log('Верно') : console.log('Неверно');

let test2 = true ;
if (test2) {
    console.log('Верно');
} else { console.log('Неверно');
}

// 11.	Дано Два массива рус и англ ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс']
// 12.	Если переменная lang = ru вывести массив на русском языке, а если en то вывести на английском языке. Сделат
// через if else и через тернарку.

let langRU="ru";
let langEN="en";
let langlet="en";
let weeksEN = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"];
let weeksRU = ["Понедельник","Вторник","Среда","Четверг","Пятница","Суббота","Воскресенье"];
if (langlet==langRU){
    console.log(weeksRU);
}
else if (langlet==langEN){
    console.log(weeksEN);
}
else{
    console.log("Неправильно введены данные");
}
console.log(langlet==langRU ? "Monday Tuesday Wednesday Thursday Friday Saturday Sunday" :
    "Понедельник Вторник Среда Четверг Пятница Суббота Воскресенье");

let en = ["sun", "mon"];
let ru = ["Пн", "Вт"];
let lg = ru;
console.log(lg == ru ? ru : en);

// 13.	В переменной лежит число от 0 до 59 – это минуты. Определите в какую четверть часа попадает это число
// (в первую, вторую, третью или четвертую). тернарка и if else.

let minuta = 60;
if (minuta >= 0 && minuta <= 14) {
    console.log('В первую четверть.');
}
if (minuta >= 15 && minuta <= 30) {
    console.log('Во вторую четверть.');
}
if (minuta >= 31 && minuta <= 45) {
    console.log('В третью четверть.');
}
if (minuta >= 46 && minuta <= 59) {
    console.log('В четвертую четверть.');
}

let minuta1 = prompt('Введите число', '');
let vrem = (minuta1 >=0 && minuta1 <= 14) ? 'В первую четверть.' :
    (minuta1 >= 15 && minuta1 <= 30) ? 'Во вторую четверть' :
        (minuta1 >= 31 && minuta1 <= 45)  ? 'В третью четверть' :
            (minuta1 >= 46 && minuta1 <= 59 || minuta == 60) ? 'В четвертую четверть ' :
                'Введите корректное значение';
console.log(vrem);