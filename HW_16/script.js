// 1. Дан массив
// ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']

// Реализовать все буквы маленькие, пример вызова метода
// test(arr, val => val.toLowerCase())

let mass = ['Alex', 'Vanya', 'Tanya', 'Lena', 'Tolya']
function test(arr, fun){
    let len = arr.length;
    let result = [];
    for(let i = 0; i < len; i++){
        result.push(fun(mass[i]))
    }
    return result
}
console.log(test(mass, val => val.toLowerCase()))


// 2. Дан массив
// [44, 12, 11, 7, 1, 99, 43, 5, 69]
// Упорядочить массив
// test(arr, val => правила сравнения asc или desc)

let mass = [44, 12, 11, 7, 1, 99, 43, 5, 69]
function test(arr, fun){
    let len = arr.length;
    for (let i = 1; i < len; i++){
        for (let j = 0; j < i; j++){
            if(fun(arr[i], arr[j])){
                let b = arr[i];
                arr[i] = arr[j];
                arr[j] = b;  
            }
        }
    }    
    return arr
}
console.log(test(mass, (i, j) => i > j))

// 3. Задача с классами. Реализовать класс User, User будет иметь конструктор 
// (firstName, secondName, lastName, age, receiptDate, dateOfIssue, specialty).
//  Реализовать два класса наследника:
// - Студент (добавить в конструктор grade => средний бал)
// - Подготовительная группа (issueOrDeduction => true or false)

// Затем реализовать методы
// - метод сокращение с поного имени на короткое (Забара Александр Сергеевич => Забара А.С.)

// - метод сделает заглавными только первые буквы
// - метод возвращает длительность учебы в годах
// - метод возвращает длительность учебы в месяцах

// - метод возвращает возраст студента
// - метод возвращает диплом или диплом не получен (100 балом макс, нет диплома меньше 60 балов)

class User {
  constructor (firstName, secondName, lastName, age, receiptDate, dateOfIssue, specialty) {
    this.firstName = firstName;
    this.secondName = secondName;
    this.lastName = lastName;
    this.age = age;
    this.receiptDate = receiptDate;
    this.dateOfIssue = dateOfIssue;
    this.specialty = specialty;
  }
  shortName(){
     return this.lastName + ' ' + this.firstName[0] + '.' + ' ' + this.secondName[0] + '.'
  }
  letterUp() {
    function toUpLet(name) {
      let i = 0, upName = " "
      while (i < name.length) {
        if (i == 0) {
          upName += name[i].toUpperCase()
        }
        else { upName += name[i] }
        i++
      }
      return upName
    }
    return toUpLet(this.secondName) + " " + toUpLet(this.firstName) + " " + toUpLet(this.lastName)
  }
  
  learningYear(){
    return this.dateOfIssue - this.receiptDate + ' лет'
  }
  
  learningMonths = function(){
    return (this.dateOfIssue - this.receiptDate) * 12 + ' месяцев'
}
diplom(){
      if(this.grade > 60 && this.grade <= 100){
          return 'Диплом'
      } else {
          return 'Нет диплома!'
      }
  }
}
const user = new User('Ольга', 'Ивановна', 'Иванова', 2002, 2017, 2021, 'Fullstack')
  
   class Student extends User {
    constructor(firstName, secondName, lastName, 
      age, receiptDate, dateOfIssue, specialty, grade) {
      super(firstName, secondName, lastName, age, 
      receiptDate, dateOfIssue, specialty);
      this.grade = grade;
    }
}

    class PreGroup extends User{
      constructor(firstName, secondName, lastName, 
        age, receiptDate, dateOfIssue, specialty, grade) {
        super(firstName, secondName, lastName, age, 
        receiptDate, dateOfIssue, specialty)
        this.issueOrDeduction = true
    }
}
let student = new Student('Ольга', 'Ивановна', 'Иванова', 2002, 2017, 2021, 'Fullstack', 90);
console.log(user.shortName());
console.log(student.diplom());