let firstName = 'Евгения'
let lastName = 'Алексеенко'
let age = 1988

alert(`${firstName} ${lastName} ${age}`)
console.log(`${firstName} ${lastName} ${age}`)

const FNAME  = 'Евгения';
const LNAME = 'Алексеенко';
const MYAGE = 1988

alert(FNAME + ' ' + LNAME + ' ' + MYAGE)
console.log(FNAME + ' ' + LNAME + ' ' + MYAGE)

let f_Name = prompt('Как тебя зовут?', 'Евгения')
alert(`Тебя зовут ${f_Name} `)
console.log(`Тебя зовут ${f_Name} `)

let l_Name = prompt('Твоя фамилия?', 'Алексеенко')
alert(`Твоя фамилия ${l_Name} `)
console.log(`Твоя фамилия ${l_Name} `)

let my_age = prompt('Сколько тебе лет?', '33')
alert(`Тебе ${my_age} лет!`)
console.log(`Тебе ${my_age} лет!`)

let yourAge = confirm("Есть ли Вам 18 лет?")
alert( yourAge ) // true, если нажата OK
alert('Доступ разрешен')
console.log(yourAge + ' ' + 'Доступ разрешен')

let now = new Date();
alert( now ); // показывает текущие дату и время
console.log(now)

let dataNow = prompt('Какая сегодня дата?', now)
alert(`Сегодня ${dataNow} `)
console.log(`Сегодня ${dataNow} `)

let a = prompt("Введите цифру")
let b = prompt("Введите цифру")

alert(a + b)
console.log(a+b)

const COLOR = "#ccc"
document.body.style.background = color

let arr= ["#ADFF2F", "#FA8072", "#FFB6C1", "#FFA07A", "#B0E0E6" ]
document.body.style.background = arr[4]

let person = {
    name: 'John',
    surname: 'Doe',
    nickname: 'Jonny',
    photo: 'https://img.gazeta.ru/files3/281/6365281/upload-kinopoisk.ru-John-Wick-2508221-pic3-700x467-24874.jpg',
    age: 35,
    gender: 'male',
    mail: 'john.john@gmail.com',
}
document.getElementById("person").innerHTML = `Name: ${person.name},<br> Surname: ${person.surname},<br>
 Nickname: ${person.nickname},<br> Photo: <img src=${person.photo} alt="Фото">, <br> Age: ${person.age}, <br>
 Gender: ${person.gender}, <br> Mail: ${person.mail}`

let car = {
    mark: 'Audi',
    brand: 'category German cars',
    model: 'S8',
    year: 2020,
    isNew: true,
    color: 'white',
    founders: 'August Horch',
    body: 'saloon',
    car_class: 'urban',
    engine_volume: 3993,
    fuel_grade: 'AI-95 gasoline',
    type_of_drive: 'four-wheel drive',
    acceleration_time: 4.1,
    maximum_speed: 250,
    number_of_seats: 5,
}
document.getElementById("car").innerHTML = `Mark: ${car.mark},<br> Brand: ${car.brand},<br>
 Model: ${car.model},<br> Year: ${car.year} , <br> isNew: ${car.isNew}, <br>
 Color: ${car.color}, <br> Founders: ${car.founders}, <br> Body: ${car.body}, <br> Car class: ${car.car_class},
 <br> Engine volume: ${car.engine_volume}, <br> Fuel grade: ${car.fuel_grade}, <br> Type of drive: ${car.type_of_drive},
 <br> Acceleration_time: ${car.acceleration_time}, <br> Maximum speed: ${car.maximum_speed}, 
 <br> Number of seats: ${car.number_of_seats}`

let house = {
    number_of_floors: 2,
    number_of_balconies: 1,
    number_of_windows: 8,
    type_of_windows: 'energy saving',
    type_of_doubleglazed_windows: 'metal-plastic windows with double-glazed window',
    wiring: 'copper wire, without electrical fittings',
    plate_type: 'electric range',
    heating_system: 'Two-zone pipe with horizontal distribution of pipelines in the thickness of the preparation under the floors',
    heating_devices_type: 'Panel steel radiators',
    house_area: 250,
    screed: 'concrete',
    warm_floor: true,
    repairs: 'renovation',
    roof: 'metal tile',
    facade_type: 'Mosaic cladding'
}
document.getElementById("house").innerHTML = `Number of floors: ${house.number_of_floors},<br> Number of balconies: ${house.number_of_balconies},<br>
 Number of windows: ${house.number_of_windows},<br> Type of windows: ${house.type_of_windows} , <br> Type of doubleglazed windows: ${house.type_of_doubleglazed_windows}, <br>
 Wiring: ${house.wiring}, <br> Plate type: ${house.plate_type}, <br> Heating system: ${house.heating_system}, <br> Heating devices type ${house.heating_devices_type}, 
 <br> House area: ${house.house_area},<br> Screed: ${house.screed}, <br> Warm floor: ${house.warm_floor},
 <br> Repairs: ${house.repairs}, <br> Roof: ${house.roof}, 
 <br> Facade type: ${house.facade_type}`